# Process Flow for requesting new Service Accounts
The form request can be initiated from this link:  
[Service Account Request](https://portal.eduweb.vic.gov.au/forms/Lists/ServiceAccountRequests/Item/template.xsn?source=&DefaultItemOpen=1)

## Main Components

### Purpose of Technical Vetting Process
1.  Review the request and decide upon a suitable account name
2.	Assess if the request should be flagged for manual processing
3.	Make any observations that might be taken into account when the AD owner is deciding to approve or reject
4.	Enter the account Description, Firstname & Lastname
5.	Requested groups are checked to ensure they exist and the request is marked for manual processing if the request includes membership of high privilege groups (DA / EA / Schema Admins)

### Files
- `Process-Newserviceaccountrequest.ps1`  
	Function to process approved requests
- `SPListFunctions.ps1`  
	Helper functions to read and write to the list. Sharepoint connectivity relies on the dll files in the includes folder being loaded into memory (called by one of the helper functions)

### Technical Vetting Steps via script
You can get the latest version of the technical vetting script [here](http://gitlab/Jonathan/techvettingserviceaccountrequests) or by cloning the repo via  

`git clone http://gitlab/Jonathan/techvettingserviceaccountrequests`  

1. Launch the `Process-Newserviceaccountrequest.ps1` script
2. Choose `R` to reset a previously vetted request, `A` to process all or a specific `number` to process that particular reqest  
3. The script will read the current list of available requests from teh RSS feed.   Requests which are in a state to perform technical vetting (request has not yet been processed but has been approved by first approver) are presented to the technical vetter.  
4. Decide upon a suitable service account name and enter that when prompted. The account name format is `<svc><descriptivename of task or application><environmnet>`  
5. Enter a suitable first and last name  
6. Enter a suitable description  
7. If there are additional technical vetting notes you would like to add  or you believe this request should be processed manually then select the appropriate option (Y or M) when prompted  
8. The summary screen will show you what updates will be made to the request prior to processing at the end. If you see something wrong then you can cancel by pressing `C` when prompted

### Technical Vetting Steps via Sharepoint
Request can be vetted or modified without using the script via the [Sharepoint list](https://portal.eduweb.vic.gov.au/forms/Lists/ServiceAccountRequests/AllItems.aspx). To do this locate the request in the list then follow these steps

1. click the `Title` link.  
2.  The request will open in the `submitted` view which allows you to change or set the `serviceaccountname`, `environment`, `group membership`, `technical vetting notes`, `technical vetting by` & `Manual Processing` fields.
3. If you are vetting via edits to the form, click the 'Edit Item' button in the top left.
4. Once in 'Edit' mode you can also switch view to the original submission view (called Edit Item)
5. Make the required changes and save the form  

