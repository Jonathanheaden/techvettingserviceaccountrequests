Import-Module ActiveDirectory
$siteuri = "https://portal.eduweb.vic.gov.au/forms"
$listname = "ServiceAccountRequests"
$rssfeed = "https://portal.eduweb.vic.gov.au/forms/_layouts/15/listfeed.aspx?List=%7B76DE217D%2D09F0%2D4081%2DA00E%2D69881636E155%7D&Source=https%3A%2F%2Fportal%2Eeduweb%2Evic%2Egov%2Eau%2Fforms%2FLists%2FServiceAccountRequests%2FAllItems%2Easpx"

$embargoedGroups = @("Domain Admins", "Enterprise Admins", "Schema Admins")

Function get-openlistitems {
    $webclient = new-object system.net.webclient
    $webclient.UseDefaultCredentials = $true
    [xml]$rs = $webclient.DownloadString($rssFeed)
    $openItems =$rs.rss.channel.item | ? {
	(
	    ($_.description.'#cdata-section' -match "RequestCompleted\:\<\/b\> No") -and
	    ($_.description.'#cdata-section' -match "approvalstatus:</b> approved by First Approver") -and
	    ($_.description.'#cdata-section' -notmatch "ServiceAccountName:")
	)
    }
    return $openitems | % { $_.link.split("=")[1] }
}

Function Invoke-LoadMethod() {
    param(
	$ClientObject = $(throw "Please provide an Client Object instance on which to invoke the generic method")
    ) 
    $ctx = $ClientObject.Context
    $load = [Microsoft.SharePoint.Client.ClientContext].GetMethod("Load") 
    $type = $ClientObject.GetType()
    $clientObjectLoad = $load.MakeGenericMethod($type) 
    $clientObjectLoad.Invoke($ctx,@($ClientObject,$null))
}

Function get-listItemByID ($id) {
    $ctx = New-Object Microsoft.SharePoint.Client.ClientContext($siteuri)
    $web = $ctx.Web
    $list =  $Web.Lists.GetByTitle($listname)
    Invoke-LoadMethod -ClientObject $list
    $ctx.ExecuteQuery()
    $SPItem = $list.GetItemById($id)
    Invoke-LoadMethod $SPItem
    $ctx.ExecuteQuery()
    $result = new-object system.object
    [REGEX]$htmlregex = "(\<div class\=`"\w+`"\>)(.*)(?<capture>`n.*)(\<)"
    if ($SPItem.FieldValues["AdditionalNotes"] -match $htmlregex) {
	$additionalnotesdisplay = $matches.capture.trim()
    } else {
	$additionalnotesdisplay = $SPItem.FieldValues["AdditionalNotes"]
    }
    if ($SPItem.FieldValues["GroupMembership"] -match $htmlregex) {
	$groupmembershipdisplay = $matches.capture.trim()
    } else {
	$groupmembershipdisplay = $SPItem.FieldValues["GroupMembership"]
    }
    add-member -inputobject $result -membertype NoteProperty -name "ID" -value  $id
    add-member -inputobject $result -membertype NoteProperty -name "PurposeOfAccount" -value  $SPItem.fieldvalues["PurposeOfAccount"]
    add-member -inputobject $result -membertype NoteProperty -name "AccountOwner" -value  $SPItem.fieldvalues["ApproverName"]
    add-member -inputobject $result -membertype NoteProperty -name "TechnicalContact" -value  $SPItem.fieldvalues["TechnicalContactName"]
    add-member -inputobject $result -membertype NoteProperty -name "Environment" -value  $SPItem.fieldvalues["Environment"]
    add-member -inputobject $result -membertype NoteProperty -name "AccountDuration" -value  $SPItem.fieldvalues["accountduration"]
    add-member -inputobject $result -membertype NoteProperty -name "Requester" -value  $SPItem.fieldvalues["Author"].LookupValue
    add-member -inputobject $result -membertype NoteProperty -name "ServicePlatform" -value  $SPItem.fieldvalues["ServicePlatform"]
    add-member -inputobject $result -membertype NoteProperty -name "ManualProcessing" -value  $SPItem.fieldvalues["automatedProcessing"]
    add-member -inputobject $result -membertype NoteProperty -name "ApprovalStatus" -value  $SPItem.fieldvalues["approvalstatus"]
    add-member -inputobject $result -membertype NoteProperty -name "GroupMembership" -value  $groupmembershipdisplay
    add-member -inputobject $result -membertype NoteProperty -name "AdditionalNotes" -value  $additionalnotesdisplay
    return $result
}

Function Load-Assemblies {
    add-type -path .\LoadDlls\Microsoft.SharePoint.Client.dll
    add-type -path .\LoadDlls\Microsoft.SharePoint.Client.Runtime.dll
}

Function Set-RequestDetails ($request,$updates) {
    $ctx = New-Object Microsoft.SharePoint.Client.ClientContext($siteuri)
    $web = $ctx.Web
    $list =  $Web.Lists.GetByTitle($listname)
    Invoke-LoadMethod -ClientObject $list
    $ctx.ExecuteQuery()
    $SPItem = $list.GetItemById($request.id)
    Invoke-LoadMethod $SPItem
    $ctx.ExecuteQuery()
    $SPItem["ServiceAccountName"] = $updates.serviceaccountName
    $SPItem["Description"] = $updates.Description
    $SPItem["Displayname"] = $updates.Displayname
    $SPItem["TechnicalVettingNotes"] = $updates.TechnicalVettingNotes
    $SPItem["GroupMembership"] = $updates.GroupMembership
    $SPItem["automatedProcessing"] = $updates.ManualProcessing
    $SPItem["TechnicalVettingBy"]=$updates.TechnicalVettingBy
    $SPItem.update()
    $ctx.ExecuteQuery()
}

Function process-request ($id) {
    $request = get-listitembyid $id
    if ($request.ApprovalStatus -notmatch "approved by First Approver") {write-host "Request $id is not in a state for tech vetting"; break }
    $request
    $updates = new-object system.object
    add-member -inputobject $updates -membertype NoteProperty -name ServiceAccountName -value ""
    add-member -inputobject $updates -membertype NoteProperty -name Description -value ""
    add-member -inputobject $updates -membertype NoteProperty -name Displayname -value ""
    add-member -inputobject $updates -membertype NoteProperty -name TechnicalVettingNotes -value ""
    add-member -inputobject $updates -membertype NoteProperty -name TechnicalVettingBy -value (get-techVetUser)
    add-member -inputobject $updates -membertype NoteProperty -name GroupMembership -value ""
    add-member -inputobject $updates -membertype NoteProperty -name ManualProcessing -value $request.ManualProcessing
    check-requestgroups $request $updates
    get-newserviceaccountname $request $updates
    get-otherrequestdetails $request $updates
    clear-host
    write-host -foregroundcolor yellow "Technical Vetting Summary`n"
    $request
    Write-host -foregroundcolor darkGreen "`nUpdating request with following details`n"
    $updates
    write-host "Type [" -nonewline
    write-host -foregroundcolor red "C" -nonewline
    $choice = read-host "] to cancel, any other key to proceed"
    if ($choice -notmatch "^c")  { set-RequestDetails $request $updates }
}

Function check-requestgroups ($request, $updates) {
    if  ($request.GroupMembership) { $grouplist = $request.GroupMembership.split("`n").trim() } else { $grouplist = @() }
    $checkedGroups = @()
    foreach ($group in $grouplist) {
	if ($embargoedGroups -contains $group){
	    $updates.ManualProcessing = "Yes"
	    $updates.TechnicalVettingNotes = "Request contains high privilige group`n"
	}
	if (check-ADGroupExists $group) {
	    if (!(validate-group $group)) {
		$updates.TechnicalVettingNotes += "Reqested group: $group not in managed groups area`n"
	    } else {
		$checkedGroups += $group
	    }
	} else {
	    $updates.TechnicalVettingNotes += "Requested group: $group doesn't exist`n"
	}
	
	$updates.GroupMembership =  [string]::join(";",$checkedGroups)			     
    }
}

Function get-newserviceaccountname ($request, $updates) {
    $suffixes = @{
	"Test" = "TST"
	"Staging" = "STG"
	"Training" ="TRN"
	"SIT" = "SIT"
	"Development" = "DEV"
    }
    $suffix = $suffixes[$request.environment]
    write-host "Account Purpose is  : " -nonewline
    write-host -foregroundcolor yellow $request.PurposeOfAccount
    write-host "Service Platform is : " -nonewline
    write-host -foregroundcolor yellow $request.ServicePlatform
    $suggestedName = sanitize-name $suffix
    $updates.ServiceAccountName = $suggestedName
} 

Function sanitize-name ($suffix) {
    if ($suffix) { write-host "$suffix will be appended to suggested name" }
    $nameUnConfirmed = $true
    while ($nameunConfirmed) {
	Write-host "`nName should start with SVC, be less than 21 chars total and have the following format:`n"
	write-host -foregroundcolor green "SVC"-nonewline
	write-host "<<" -nonewline
	write-host -foregroundcolor darkcyan "descriptive name of task or application" -nonewline
	write-host ">><<" -nonewline
	write-host -foregroundcolor darkgreen "environment DEV/TEST/ST" -nonewline
	write-host ">>`n"
	write-host "Don't include an environment suffix with your suggested name, this will be added if necessary`n"
	$accname = read-host "What is your suggested name?"
	$suggestedName = "$accname$suffix"
	if ($suggestedName -notmatch "^svc") { write-host -foregroundcolor Red "`nName should start with SVC" }
	elseif ($suggestedName.contains(" ")) { write-host -foregroundcolor Red "`nName cannot contain spaces" }
	elseif ($suggestedName.length -gt 20) { write-host -foregroundcolor Red "`n$suggestedName is too long" }
	elseif (check-ADUserExists $suggestedName) {
	    write-host -foregroundcolor Red "`n$suggestedName already exists"
	    $similarnames = get-similarnamedusers $suggestedname
	    if ($similarnames) {
		$todisplay = @()
		$i=0
		$similarnames | % {
		    if (($i % 4) -eq 0) {$todisplay += @("`n",$_.padright(20))} else {$todisplay += $_.padright(20) }
		    $i++
		}
		write-host "The following names also exist"
		write-host -foregroundcolor Red ([string]::join(" ",$todisplay))
	    }
	}
	else {$nameunConfirmed = $false}
	$suggestedName.toCharArray()  | % {
	    if ( $_ -notmatch "\w|[-_ ]" ) {
		write-host -foregroundcolor Red ("`n$suggestedName cannot contain " + $_)
		$nameUnconfirmed = $true
	    }
	}
    }
    return $suggestedName
}

Function get-otherrequestdetails ($request, $updates) {
    $Description = read-host "What should the account description be?"
    $updates.description = $description
    write-host "`nThe first name for service accounts are normally related to the use case of the account"
    $firstname = read-host "What should the account firstname be?"
    write-host "`nThe last name for service accounts are normally related to the technical platform"
    $lastname = read-host "What should the account lastname be?"
    $Displayname = "$lastname, $firstname"
    $updates.displayname = $displayname
    write-host "`nDo you want to add other technical vet notes or mark the request for manual processing ?"
    write-host "Type " -nonewline
    write-host -foregroundcolor yellow "Y" -nonewline
    write-host " for yes, " -nonewline
    write-host -foregroundcolor yellow "M " -nonewline
    $choice = read-host "for manual processing (you'll be prompted to add explanatory notes) or any other key for no"
    if ($choice -match "^y|m") {
	if ($choice -match "^m") { $updates.ManualProcessing = "Yes" }
	$ExtraTechnicalVettingNotes = read-host "What are the additional Tech vet notes"
	$currentNotes = $updates.TechnicalVettingNotes
	$updates.TechnicalVettingNotes = "$ExtraTechnicalVettingNotes`n$currentNotes"
    }
}

Function check-ADUserExists ($ADUser) {
    try {if (get-aduser $ADUser) {$True}} catch {$False}
}

Function check-ADGroupExists ($ADGroup) {
    try {if (get-adgroup $ADGroup) {$True}} catch {$False}
}
Function get-similarnamedusers ($ADUser) {
    $svcAccNameRegex = "(ServiceAccountName:<\/b>\s*)(?<name>[\w-_]+)"
    $accounts = invoke-expression "get-aduser -f {samaccountname -like '$ADUser*'} "
    $accounts = $accounts |? {$_.samaccountname -ne $aduser }| % { $_.samaccountname }
    $otherRequests = @()
    $rs.rss.channel.item | % {
	if ($_.description.'#cdata-section' -match $svcAccNameRegex) { $name = $matches.name }
	if ($_.description.'#cdata-section' -match "approvalstatus:<\/b> (A|a)pproved" ) { $approved = $true }
	if ($approved -and ($name -match $aduser)) { $otherRequests += $name } else {$name=$approved=$false}
    }
    return ($accounts + $otherRequests) | select -unique
			     
}

Function validate-group ($adGroup) {
    $group = get-adgroup $adGroup
    $validOU = $false
    $locations = @(
	'OU=SecuredGroups,DC=education,DC=vic,DC=gov,DC=au',
	'OU=Groups,OU=Corporate,DC=education,DC=vic,DC=gov,DC=au',
	'OU=Groups,OU=Main,OU=Corporate,DC=education,DC=vic,DC=gov,DC=au'
    )
    foreach ($loc in $locations) { if ($group.DistinguishedName -match $loc) {$validOU = $true }}
    return $validOU
}

Function get-techVetUser {
    $TVuser = get-aduser (get-childitem ENV:\USERNAME).value
    return [string]::join(" ",$tvuser.givenname,$tvuser.surname)
}
