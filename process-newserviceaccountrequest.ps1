clear-host
. .\includes\SPListFunctions.ps1

#initialise 
load-assemblies

#Get the list of open requests
$requests = get-openlistitems
if (!($requests)) { Write-host "No active requests found"
		  	$requestnum = read-host "Is there a specific request number you wish to reset? Enter the number to reset or any other key to quit"
		    if ($requestnum -notmatch "^\d+$") { write-host "$requestnum is not a number, process will now end"; break }
		    process-request $requestnum
		  } else {
    Write-host ( "The number of requests ready for Technical Vetting is "+ $requests.count )
    write-host "Do you want to process a particular request ID ?`nType the [" -nonewline
    write-host -foregroundcolor yellow "number" -nonewline
    write-host "] for that ID, `n[" -nonewline
     write-host -foregroundcolor yellow "R" -nonewline
    write-host "] to reset a particular ID (only those not currently complete can be reset), `n[" -nonewline
    write-host -foregroundcolor yellow "A" -nonewline
    $choice = read-host "] for All or any other key to cancel"
    if ($choice -match "(?<requestid>^\d+$)") { process-request $matches.requestid }
    elseif ($choice -match "^r$") {
	$requestnum = read-host "What request number do you wish to reset? "
	if ($requestnum -notmatch "^\d+$") { write-host "$requestnum is not a number"; break }
	process-request $requestnum
    }
    elseif ($choice -match "^a$") {
	foreach ($requestid in $requests) {
	    Clear-Host
	    Write-host ( "The number of requests ready for Technical Vetting is "+ $requests.count )
	    Write-Host "This is requestID $requestid`n"
	    get-listitembyID $requestid
	    write-host "Type [" -nonewline
	    write-host -foregroundcolor yellow "S" -nonewline
	    $choice = read-host "] to Skip or any other key to process"
	    if ($choice -notmatch "^s") {
		clear-host
		process-request $requestid
	    }
	}
    }
}



   
